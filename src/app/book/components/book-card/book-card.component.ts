import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  Pipe,
  PipeTransform,
} from '@angular/core';
import { Router } from '@angular/router';
import { Book } from '../../models/book';

@Component({
  selector: 'app-book-card',
  templateUrl: './book-card.component.html',
  styleUrls: ['./book-card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BookCardComponent implements OnInit {
  customStyle = {
    color: 'blue',
    fontWeight: 'bold',
  };

  @Input() content: Book | undefined;

  @Output() detailClick = new EventEmitter<Book>();

  constructor(private changeDetector: ChangeDetectorRef) {
    // trigger change detection manually
    // this.changeDetector.markForCheck();
  }

  ngOnInit(): void {
    if (this.content) {
      console.log(this.content.title);
    }
    console.log(this.content?.title);
  }

  handleDetailClick(event: MouseEvent) {
    console.log('Detail click', event);
    event.preventDefault();
    this.detailClick.emit(this.content);
  }

  printAuthor(author: string) {
    console.log('Printing author');
    return `Author ${author}`;
  }
}
