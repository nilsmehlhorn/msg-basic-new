import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BookComponent } from './pages/book/book.component';
import { BookCardComponent } from './components/book-card/book-card.component';
import { AuthorPipe } from './pipes/author.pipe';
import { BookDetailComponent } from './pages/book-detail/book-detail.component';
import { BookRoutingModule } from './book-routing.module';

@NgModule({
  declarations: [
    BookComponent,
    BookCardComponent,
    AuthorPipe,
    BookDetailComponent,
  ],
  imports: [CommonModule, BookRoutingModule],
  exports: [BookComponent],
})
export class BookModule {}
