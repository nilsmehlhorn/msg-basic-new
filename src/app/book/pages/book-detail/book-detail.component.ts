import { Component, OnDestroy, OnInit } from '@angular/core';
import { BookApiService } from '../../providers/book-api.service';
import { ActivatedRoute } from '@angular/router';
import { Book } from '../../models/book';
import { EMPTY, Observable, Subscription } from 'rxjs';
import { switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-book-detail',
  templateUrl: './book-detail.component.html',
  styleUrls: ['./book-detail.component.scss'],
})
export class BookDetailComponent implements OnInit, OnDestroy {
  book$: Observable<Book> = EMPTY;

  // subscription: Subscription | undefined

  constructor(private route: ActivatedRoute, private bookApi: BookApiService) {}

  ngOnInit(): void {
    // let apiSubscription: Subscription | undefined
    // this.route.params.subscribe(params => {
    //   if (apiSubscription) {
    //     apiSubscription.unsubscribe();
    //   }
    //   const isbn = params.isbn
    //   apiSubscription = this.bookApi.getByIsbn(isbn).subscribe(bookFromApi => {
    //     this.book = bookFromApi
    //   })
    // })

    // this.subscription = this.route.params
    //   .pipe(switchMap((params) => this.bookApi.getByIsbn(params.isbn)))
    //   .subscribe((bookFromApi) => (this.book = bookFromApi));

    this.book$ = this.route.params
      .pipe(switchMap((params) => this.bookApi.getByIsbn(params.isbn)))
  }

  ngOnDestroy() {
    // this.subscription?.unsubscribe()
  }
}
