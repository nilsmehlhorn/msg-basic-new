import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { finalize } from 'rxjs/operators';
import { Book } from '../../models/book';
import { BookApiService } from '../../providers/book-api.service';

@Component({
  selector: 'app-book',
  templateUrl: './book.component.html',
  styleUrls: ['./book.component.scss'],
})
export class BookComponent {
  books: Book[] = [];
  loading = false;

  constructor(private bookApi: BookApiService, private router: Router) {
    this.loading = true;
    this.bookApi
      .getAll()
      .pipe(
        finalize(() => (this.loading = false))
      )
      .subscribe((bookFromApi) => {
        this.books = bookFromApi;
      });
  }

  handleDetailClick({isbn}: Book): void {
    this.router.navigate(['/books', 'details', isbn])
    // this.router.navigateByUrl(`/books/details/${isbn}`)
  }

  trackBook(index: number, book: Book) {
    // return book.isbn
  }
}
