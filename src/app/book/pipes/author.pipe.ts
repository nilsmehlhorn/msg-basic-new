import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'author' })
export class AuthorPipe implements PipeTransform {
  transform(author: string) {
    console.log('Printing author');
    return `Author ${author}`;
  }
}
