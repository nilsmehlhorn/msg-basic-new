import { InjectionToken } from '@angular/core';
import { environment } from '../../../environments/environment';
import { Environment } from '../../../environments/ienvironment';

export const ENV = new InjectionToken('env');

export const getEnvironment = (): Environment => {
  return environment;
};
