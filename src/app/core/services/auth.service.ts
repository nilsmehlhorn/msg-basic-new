import { Inject, Injectable } from '@angular/core';
import { Environment } from '../../../environments/ienvironment';
import { ENV } from './environment.provider';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(@Inject(ENV) private env: Environment) {
    if(env.production) {
      // TODO: login gegen AWS
    } else {
      // TODO: login gegen lokal
    }
   }
}
