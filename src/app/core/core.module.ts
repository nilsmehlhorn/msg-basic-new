import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ENV, getEnvironment } from './services/environment.provider';

@NgModule({
  declarations: [],
  imports: [CommonModule],
  providers: [
    {
      provide: ENV,
      useFactory: getEnvironment,
    },
  ],
})
export class CoreModule {}
